# 1. Implement function fo calculate of a triangle write JUnit tests for it.
- method to implement: com.itreflection.project15.learning.task1.impl.TriangleCalculationServiceImpl.calculatePerimeter
- test class in which tests should be implemented:  com.itreflection.project15.learning.task1.impl.TriangleCalculationServiceImplTest
- example of simple JUnit can be found here: com.itreflection.project15.learning.first.ClassATest

When writing a test please please take into account that parameter can be any integer.
e.g. [1,2,3], [-1,-2-3], [0,0,0], [1,1,1]



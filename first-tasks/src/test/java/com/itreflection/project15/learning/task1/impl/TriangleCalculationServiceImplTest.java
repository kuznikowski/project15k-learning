package com.itreflection.project15.learning.task1.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.slf4j.LoggerFactory.getLogger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.itreflection.project15.learning.task1.api.TriangleParameterException;

@RunWith(MockitoJUnitRunner.class)
public class TriangleCalculationServiceImplTest {

  private final Logger logger = getLogger(TriangleCalculationServiceImplTest.class);

  @InjectMocks
  private TriangleCalculationServiceImpl triangleCalculationService;

  @Test
  public void shouldCalculatePerimeterForCorrectParameters() throws Exception {
    //given
    int a = 1, b = 2, c = 2;

    //when
    double result = triangleCalculationService.calculatePerimeter(a, b, c);

    //then
    assertThat(result).isEqualTo(5);
  }

  @Test
  public void shouldThrowExceptionSideTooShort() {
    //given
    int a = -2, b = 6, c = 0;

    //when
    try {
      double result = triangleCalculationService.calculatePerimeter(a, b, c);
    } catch (Exception e) {
      // then
      assertThat(e.getMessage()).isEqualTo("At least one side is equal to or less than zero.");
    }
  }

  @Test
  public void shouldThrowExceptionPerimeterTooShort() throws Exception {
    //given
    int a = 2, b = 3, c = 90;

    //when
    try {
      double result = triangleCalculationService.calculatePerimeter(a, b, c);
    } catch (Exception e) {
      // then
      assertThat(e.getMessage()).isEqualTo("Perimeter of the triangle is too short.");
    }
  }

}
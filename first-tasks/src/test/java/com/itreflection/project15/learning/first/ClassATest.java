package com.itreflection.project15.learning.first;

import static org.fest.assertions.Assertions.assertThat;
import static org.slf4j.LoggerFactory.getLogger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

@RunWith(MockitoJUnitRunner.class)
public class ClassATest {

  private final Logger logger = getLogger(ClassATest.class);

  @InjectMocks
  private ClassA classA;

  @Test
  public void shouldReturnTrue() throws Exception {
    //given

    //when
    boolean result = classA.isTrue();
    logger.info("Result: {}", result);

    //then
    assertThat(result).isTrue();
  }

}
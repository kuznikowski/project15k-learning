package com.itreflection.project15.learning.task1.api;

public class TriangleParameterException extends RuntimeException {

  public TriangleParameterException(String message) {
    super(message);
  }
}

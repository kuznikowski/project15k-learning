package com.itreflection.project15.learning.task1.impl;

import com.itreflection.project15.learning.task1.api.TriangleCalculationService;

public class TriangleCalculationServiceImpl implements TriangleCalculationService {

  @Override
  public double calculatePerimeter(int a, int b, int c) {
    double result;
    if (a <= 0 || b <= 0 || c <= 0) {
      throw new RuntimeException("At least one side is equal to or less than zero.");
    }

    if (a < b + c || b < a + c || c < a + b ){
      throw new RuntimeException("Perimeter of the triangle is too short.");
    } else {
      result = a + b + c;
    }
  }

  return result;
}
